
class Robot {

     constructor(x, y, direction) {

          //check coordinates of robot
          if (!isNaN(x) && !isNaN(y)) {
               this.x = x;
               this.y = y;
          }
          else {
               this.x = 0;
               this.y = 0;
          }

          //check direction of robot
          //if direction argument is illegal then default is left
          switch (direction) {
               case 'right':
                    this.direction = 'right';
                    break;
               case 'down':
                    this.direction = 'down';
                    break;
               case 'up':
                    this.direction = 'up';
                    break;
               default:
                    this.direction = 'left';
          }

          this.range = 3;
          this.map = [];      //here is map, it is going to update later
     }

     //method moving of robot
     left() {
          this.x--;
          this.direction = 'left';
     }

     right() {
          this.x++;
          this.direction = 'right';
     }

     up() {
          this.y--;
          this.direction = 'up';
     }

     down() {
          this.y++;
          this.direction = 'down';
     }

     // value 1 is zombie
     // if zombie is hit shoot, zombie is dead
     shoot() {
          switch (this.direction) {
               case 'left':
                    for (let i=1; i <= this.range; i++) {
                         if (this.map[this.x-i][this.y] == 1) {
                              this.map[this.x-i][this.y] = 0;
                              break;
                         }
                    }
                    break;
               case 'right':
                    for (let i=1; i <= this.range; i++) {
                         if (this.map[this.x+i][this.y] == 1) {
                              this.map[this.x+i][this.y] = 0;
                              break;
                         }
                    }
                    break;
               case 'down':
                    for (let i=1; i <= this.range; i++) {
                         if (this.map[this.x][this.y+i] == 1) {
                              this.map[this.x][this.y+i] = 0;
                              break;
                         }
                    }
                    break;
               case 'up':
                    for (let i=1; i <= this.range; i++) {
                         if (this.map[this.x][this.y-i] == 1) {
                              this.map[this.x][this.y-i] = 0;
                              break;
                         }
                    }
                    break;
          }
     }
}


let robot = new Robot(19, 5, 'down');

////////////////// updating //////////////////////
